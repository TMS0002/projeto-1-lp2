<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class principal extends CI_Controller {
	
	public function home()
	{
		$this->load->view('commom/header');
		$this->load->view('commom/navbar');
		$this->load-> view('pagina/home');
		$this->load->view('commom/footer');  
	}

		public function about()
	{
        $this->load->view('commom/header');
		$this->load->view('commom/navbar');
		$this->load->view('pagina/about');
		$this->load->view('commom/footer');
	}
	
	public function galeria()
    {
        $this->load->view('commom/header');
        $this->load->view('commom/navbar');
        $this->load->view('pagina/galeria');
        $this->load->view('commom/footer');        
    }


}
?>