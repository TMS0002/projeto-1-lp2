  <!-- Navbar -->
  <nav class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
    <div class="container">

      <!-- Brand -->
      <a class="navbar-brand" href="http://localhost/projeto-1-lp2/lp2/principal/home" target="_blank">
        <img <?= base_url('img/nav-logo.jpg') ?>>
        <strong>Counter-Strike Global Offensive</strong>
      </a>


      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>


      <div class="collapse navbar-collapse" id="navbarSupportedContent">

        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="http://localhost/projeto-1-lp2/lp2/principal/home">Pagina Inicial </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="http://localhost/projeto-1-lp2/lp2/principal/galeria">Galeria</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="http://localhost/projeto-1-lp2/lp2/principal/about" >Sobre o jogo</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="https://store.steampowered.com/app/730/CounterStrike_Global_Offensive/">download</a>
          </li>
        </ul>

        <ul class="navbar-nav nav-flex-icons">
          <li class="nav-item">
            <a href="https://web.facebook.com/CounterStrike/" class="nav-link" target="_blank">
              <i class="fab fa-facebook-f"></i>
            </a>
          </li>
          <li class="nav-item">
            <a href="" class="nav-link" target="_blank">
              <i class="fab fa-instagram"></i>
            </a>
          </li>
        </ul>

      </div>

    </div>
  </nav>
  <!-- Navbar -->