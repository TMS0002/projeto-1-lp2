
    <footer class="page-footer text-center font-small mdb-color darken-2 mt-4 wow fadeIn">

        <hr class="my-4">

        <div class="pb-4">
            <a href="https://web.facebook.com/CounterStrike/" target="_blank">
                <i class="fab fa-facebook-f mr-3"></i>
            </a>

            <a href="https://www.instagram.com/csgo_dev/?hl=pt-br" target="_blank">
                <i class="fab fa-instagram mr-3"></i>
            </a>
        </div>

        <div class="footer-copyright py-3">
            © 2019 Copyright:
            <a href="https://www.valvesoftware.com/pt-br/" target="_blank">Valve Corporation.com </a>
        </div>
        <!--/.Copyright-->

    </footer>
    <!--/.Footer-->