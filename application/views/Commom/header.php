<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Counter-Strike:Global Offensive</title>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"> 
  <link rel="icon" href="<?=base_url('assets/img/csgo-logo.jpg')?>" />
  <link href="<?= base_url('')?>assets/mdb/css/custom.css" rel="stylesheet">
  <link href="<?= base_url('')?>assets/mdb/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?= base_url('')?>assets/mdb/css/mdb.min.css" rel="stylesheet">
  <link href="<?= base_url('')?>assets/mdb/css/style.css" rel="stylesheet">
  <style type="text/css">
    /* Necessary for full page carousel*/
    html,
    body,
    header,
    .view {
      height: 100%;
    }

    /* Carousel*/
    .carousel,
    .carousel-item,
    .carousel-item.active {
      height: 100%;
    }
    .carousel-inner {
      height: 100%;
    }

    @media (min-width: 800px) and (max-width: 850px) {
      .navbar:not(.top-nav-collapse) {
          background: #1C2331!important;
      }
  }

  </style>
</head>

<body>