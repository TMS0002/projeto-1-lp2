<!DOCTYPE html>
<html lang="en">

<body>


    <main class="mt-5 pt-5">
        <div class="container">


            <section class="mb-4">


                <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">
                
                    <div class="carousel-inner" role="listbox">
                     
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="https://pbs.twimg.com/media/DT09QQvXcAA_Sup.jpg" alt="First slide">
                        </div>

                    </div>

                </div>


            </section>

            <section class="text-center">


                <div class="row">


                    <div class="col-lg-4 col-md-12 mb-3">

                        <div class="view overlay z-depth-1-half">
                            <img src="http://i.imgur.com/Yul7HuG.jpg" class="img-fluid" alt="">
                            <a>
                                <div class="mask rgba-white-light"></div>
                            </a>
                        </div>

                    </div>

                    <div class="col-lg-4 col-md-6 mb-3">

                        <div class="view overlay z-depth-1-half">
                            <img src="http://i.imgur.com/Ii9Q1SC.jpg" class="img-fluid" alt="">
                            <a>
                                <div class="mask rgba-white-light"></div>
                            </a>
                        </div>

                    </div>

                    <div class="col-lg-4 col-md-6 mb-3">

                        <div class="view overlay z-depth-1-half">
                            <img src="https://www.csgowallpapers.com/assets/images/original/mossawi_44404773137_20181106165233_778360609217.jpg" class="img-fluid" alt="">
                            <a>
                                <div class="mask rgba-white-light"></div>
                            </a>
                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="col-md-6 mb-3">

                        <div class="view overlay z-depth-1-half">
                            <img src="https://i.ytimg.com/vi/0P389pv1M_s/maxresdefault.jpg" class="img-fluid" alt="">
                            <a>
                                <div class="mask rgba-white-light"></div>
                            </a>
                        </div>

                    </div>
 
                    <div class="col-md-6 mb-3">

                        <div class="view overlay z-depth-1-half">
                            <img src="https://www.talkesport.com/wp-content/uploads/Untitled-1-90.jpg" class="img-fluid" alt="">
                            <a>
                                <div class="mask rgba-white-light"></div>
                            </a>
                        </div>

                    </div>
    

                </div>
     
                <div class="row">

                    <div class="col-md-12 mb-3">

                        <div class="view overlay z-depth-1-half">
                            <img src="https://skins.cash/blog/wp-content/uploads/2018/07/CS-GO-wallpaper-HD-Maps.jpg" class="img-fluid" alt="">
                            <a>
                                <div class="mask rgba-white-light"></div>
                            </a>
                        </div>

                    </div>
              

                </div>
        


            </section>
        



         
            <section class="card wow fadeIn" style="background-image: url(https://mdbootstrap.com/img/Photos/Others/background2.jpg);">

 
                <div class="card-body text-white text-center py-5 px-5 my-5">

                    <h1 class="mb-4">
                        <strong>Alguns dos mapas, jogadores e arenas de campeonatos </strong>
                    </h1>
                    <p>
                        <strong>O jogo tem um vasto cenário competitivo que ficou ainda mais forte com o passar do tempo</strong>
                    </p>
                    <p class="mb-4">
                        <strong>Atualmente no Brasil a diversos times profissionais e amadores,tanto masculino quanto feminino que sonham em um dia disputar grandes campeonatos o maior deles sendo o Major</strong>
                    </p>

                </div>
        
            </section>
          

        </div>
    </main>

</body>

</html>
