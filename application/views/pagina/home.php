<!DOCTYPE html>
<html lang="en">

<body>

    <main class="mt-5 pt-5">
        <div class="container">

            <section class="card blue-gradient wow fadeIn" id="intro">

                <div class="card-body text-white text-center py-5 px-5 my-5">

                    <h1 class="mb-4">
                        <strong>Bem vindo ao site sobre Counter-Strike Global Offensive</strong>
                    </h1>
                    <p>
                        <strong>Popularmente chamado pelos jogadores apenas de CSGO</strong>
                    </p>
                    <p class="mb-4">
                        <strong>Aqui você ira conhecer um pouco mais sobre o jogo e podera baixalo também caso se interesse e goste do jogo</strong>
                    </p>

                </div>
            </section>

            <section class="pt-5">

                <div class="wow fadeIn">

                    <h2 class="h1 text-center mb-5">Jogada incriveis e mapas mais famosos</h2>

                    <p class="text-center">As melhores jogadas são sem duvidas as dos jogadores profissionais, pois eles fazem coisas consideradas tecnicamente impossiveis mas eles mostram o contrario. </p>
                    <p class="text-center mb-5 pb-5">Existem mapas consagrados
                        <strong>na historia</strong> do CSGO, dois deles são a the_dust2 e the_nukeque são alguns dos mapas maias famosos que existem desde o começo do jogo Counter-Strike, muito antes do Global Offensive surgir.</p>
                </div>

                <div class="row wow fadeIn">

                    <div class="col-lg-5 col-xl-4 mb-4">

                        <div class="view overlay rounded z-depth-1-half">
                            <div class="view overlay">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://thumbs.gfycat.com/ScentedJadedHalicore-max-1mb.gif" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-7 col-xl-7 ml-xl-4 mb-4">
                        <h3 class="mb-3 font-weight-bold dark-grey-text">
                            <strong>Esssa é considerada a melhor jogada de um jogador profissionais brasileiro de CSGO.</strong>
                        </h3>
                        <p class="grey-text">Os narradores americanos são muito elogiados pela narração empolgada e que transmite muita emoção a quem assiste os jogos.</p>
                        <p>
                            <strong>No Brasil há diversos jogadores profissionais mas nenhum deles são melhores que os atuais jogadores do time da Immortal,  MIBR.</strong>
                        </p>
                        <a href="https://www.youtube.com/watch?v=cXTThxoywNQ"></a>
                    </div>
    

                </div>

                <hr class="mb-5">

                <div class="row mt-3 wow fadeIn">

                    <div class="col-lg-5 col-xl-4 mb-4">

                        <div class="view overlay rounded z-depth-1">
                            <img src="https://gaming-tools.com/counterstrike/wp-content/uploads/sites/3/2018/07/epic-colorful-dust-2-counterstrike-go-wallpaper.jpg" class="img-fluid" alt="Dust2">
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>
                    </div>


                    <div class="col-lg-7 col-xl-7 ml-xl-4 mb-4">
                        <h3 class="mb-3 font-weight-bold dark-grey-text">
                            <strong>Esse é o mapa the_dust2, variante do mapa the_dust que é maior.</strong>
                        </h3>
                        <p class="grey-text">Mapa favorita da comunidade de CSGO e muito querido por todos desde sua criação.</p>

                        </a>
                    </div>

                </div>

                <hr class="mb-5">

                <div class="row wow fadeIn">

                    <div class="col-lg-5 col-xl-4 mb-4">

                        <div class="view overlay rounded z-depth-1">
                            <img src="http://cdn.vs.com.br/vs-img/1518183377962-nuke.jpg" class="img-fluid" alt="">
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-7 col-xl-7 ml-xl-4 mb-4">
                        <h3 class="mb-3 font-weight-bold dark-grey-text">
                            <strong>Um dos mapas mais antigos de CS juntamente com Dust2 que se passa em uma usina de energia nuclear.</strong>
                        </h3>
                        <p class="grey-text">Nuke é considerado um dos mapas mais bonitos do CSGO porém com tanta qualidade grafíca
                            também é considerado um dos mais pesados tanto para quem tem um PC potente e principalmente um pc fraco.</p>
                        </a>
                    </div>

                </div>

                <hr class="mb-5">



            </section>

        </div>
    </main>
    
</body>

</html>