<!DOCTYPE html>
<html lang="en">


<body>

  <div id="carousel-example-1z" class="carousel slide carousel-fade">

    <div class="carousel-inner" role="listbox">

      <div class="carousel-item active">
        <div class="view">

        <div class="carousel-item active">
          <img class="d-block w-100" src="https://wallpapercave.com/wp/wp2373289.png">
        </div>

          <div class="mask rgba-black-light d-flex justify-content-center align-items-center">

            <div class="text-center white-text mx-5 wow fadeIn">
              <h1 class="mb-4">
                <strong>Counter-Strike Global Offensive</strong>
              </h1>

              <p>
                <strong>No jogo a uma grande variedade de armas</strong>
              </p>

              <p class="mb-4 d-none d-md-block">
                <strong>Diferentes mapas e cenarios em diferentes regiões ao redor do mundo</strong>
              </p>

            </div>

          </div>

        </div>
      </div>


        </div>
      </div>

    </div>

  </div>

  <main>
    <div class="container">

      <section class="mt-5 wow fadeIn">

        <div class="row">

          <div class="col-md-6 mb-4">

            <img src="https://sm.ign.com/ign_br/blogroll/t/the-worlds/the-worlds-best-counter-strike-teams-are-competing_pttt.jpg" class="img-fluid z-depth-1-half" alt="">

          </div>

          <div class="col-md-6 mb-4">

            <h3 class="h3 mb-3">No jogo você pode jogar como terroristas ou contra-terroristas</h3>
            <p>Onde seu objetivo como terrorista é invadir um dos dois bombsites (
              <strong>A ou B</strong> )</p>
            <p>Após invadi-lo, você deve plantar a bomba e se defender dos contra-terroristas.</p>
            <p>E como contra-terroristas seu objetivo é defender o bombsite dos terrorista por um periodo de tempo, e caso invadam seu objetivo é retomar o bombsite matando todos os terroristas e desarmando a bomba.</p>


            <p>
               O time é composto por 5 jogadores, onde cada um deles pode desempenhar uma função.
              utilizando armas como rifles de assalto, rifles precisão, submetralhadoras, escopetas e pistolas</p>

              <hr>

            <a target="_blank" href="https://store.steampowered.com/app/730/CounterStrike_Global_Offensive/" class="btn btn-grey btn-md">Download
              <i class="fas fa-download ml-1"></i>
            </a>

          </div>

        </div>

      </section>

      <hr class="my-5">

      <section>

        <h3 class="h3 text-center mb-5">GAME MODES</h3>

        <div class="row wow fadeIn">

          <div class="col-lg-6 col-md-12 px-4">

            <div class="row">
              <div class="col-1 mr-3">
              </div>
              <div class="col-10">
                <h5 class="feature-title">Casual</h5>
                <p class="grey-text">No modo casaul, existem os modos Desarme e Reféns.</p>
              </div>
            </div>

            <div style="height:30px"></div>

            <div class="row">
              <div class="col-1 mr-3">
              </div>
              <div class="col-10">
                <h5 class="feature-title">Braço direito</h5>
                <p class="grey-text">Braço direito consite em duas duplas sendo 2 jogadores de CTs e 2 jogadores de TRs.</p>
              </div>
            </div>

            <div style="height:30px"></div>

            <div class="row">
              <div class="col-1 mr-3">
              </div>
              <div class="col-10">
                <h5 class="feature-title">Competivo</h5>
                <p class="grey-text">E por fim o modo competitivo, que nada mais é o modo 5x5 dos CTs contra os TRs, tanto no modo desarme quanto no modo reféns.</p>
              </div>
            </div>

          </div>

          <div class="col-lg-6 col-md-12">

            <p class="h5 text-center mb-4">Menu panoramico do jogo</p>
            <div class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" src="http://media.steampowered.com/apps/csgo/blog/images/posts/panoramaUI.jpg" allowfullscreen></iframe>
            </div>
          </div>

        </div>

      </section>

      <hr class="my-5">

      <section>

        <h2 class="my-5 h3 text-center">Ainda não é o bastante?</h2>

        <div class="row features-small mb-5 mt-3 wow fadeIn">

        

          <div class="col-md-4">
            <div class="row">
              <div class="col-10">
                <h6 class="feature-title">Mata-Mata</h6>
                <p class="grey-text">O modo Mata-mata consiste em dois times Terroristas(TR) e Contra-Terroristas(CT) um contra o outro, aquele que no final do tempo determiando tiver mais tempo é determinado o vencendor.</p>
                <div style="height:15px"></div>
              </div>
            </div>

            <div class="row">
              <div class="col-10">
                <h6 class="feature-title">Jogos de Guerra</h6>
                <p class="grey-text">No modo jogos de guerra, existem outros três sub modos, sendo eles corrida armada, demolição e mata pombo.</p>
                <div style="height:15px"></div>
              </div>
            </div>

            <div class="row">
              <div class="col-10">
                <h6 class="feature-title">Zona de perigo</h6>
                <p class="grey-text">Zona de perigo é um modo que foi introduzido a pouco tempo, por essa nova moda de jogos estilo Battle Royale.</p>
                <div style="height:15px"></div>
              </div>
            </div>


          </div>

          <div class="col-md-4 flex-center">
            <img src="https://www.researchgate.net/profile/Ana_Regina_Cuperschmid/publication/273143941/figure/fig7/AS:294824243679236@1447302985327/Objetivos-de-longo-prazo-variam-de-acordo-com-o-modo-de-jogo-escolhido-Fonte_Q320.jpg" alt="MDB Magazine Template displayed on iPhone" class="z-depth-0 img-fluid">
          </div>

        </div>

      </section>

      <hr class="mb-5">

      <section>

        <h2 class="my-5 h3 text-center">Sobre os modos</h2>

        <div class="row features-small mt-5 wow fadeIn">

          <div class="col-xl-3 col-lg-6">
            <div class="row">
              <div class="col-10 mb-2 pl-3">
                <h5 class="feature-title font-bold mb-1">Zona de Perigo</h5>
                <p class="grey-text mt-2">um modo de jogo estilo Battle Royale frenético baseado na jogabilidade tática do CS:GO, onde jogadores usam a sua astúcia, habilidade e recursos para lutar até o final.</p>
              </div>
            </div>
          </div>

          <div class="col-xl-3 col-lg-6">
            <div class="row">
              <div class="col-10 mb-2">
                <h5 class="feature-title font-bold mb-1">Corrida Armada</h5>
                <p class="grey-text mt-2">O modo Corrida às Armas é um modo de progressão de armas onde reentras em jogo instantaneamente e onde também acabas muitas vezes por enfrentar os teus adversários de perto. Os jogadores são equipados com uma arma nova imediatamente após matarem um adversário. Mata um adversário com a última arma da sequência, uma faca dourada, para ganhares a partida.</p>
              </div>
            </div>
          </div>

          <div class="col-xl-3 col-lg-6">
            <div class="row">
              <div class="col-10 mb-2">
                <h5 class="feature-title font-bold mb-1">Demolição</h5>
                <p class="grey-text mt-2">No modo Demolição, os jogadores jogam várias rondas de ataque e defesa de um único alvo de bomba numa série de mapas concebidos para partidas rápidas. Os jogadores recebem automaticamente uma arma inicial e avançam através de uma sequência de armas sempre que matarem um adversário. Se achares que estiveres à altura do desafio, podes tentar matar um adversário em cada ronda e alcançar o patamar das armas derradeiras, as poderosas espingardas de sniper!
                </p>
              </div>
            </div>
          </div>

          <div class="col-xl-3 col-lg-6">
            <!--Grid row-->
            <div class="row">

              <div class="col-10 mb-2">
                <h5 class="feature-title font-bold mb-1">Mata Pombo</h5>
                <p class="grey-text mt-2">O modo Mata Pombo é um mata-mata porém com apenas uma das armas disponiveis no jogo o rifle de precisão SSG, nesse modo a gravidade dos jogadores é alterada para que pulem muito mais alto, por isso, mata pombo.
                </p>
              </div>
            </div>
          </div>

        </div>

        <div class="row features-small mt-4 wow fadeIn">

          <div class="col-xl-3 col-lg-6">
            <div class="row">

              <div class="col-10 mb-2">
                <h5 class="feature-title font-bold mb-1">Casual reféns</h5>
                <p class="grey-text mt-2">O modo casual com reféns nada mais é do que modo onde 12 jogadores sendo 6 TRs e 6 CTs são colocados um contra o outro, onde os Terroristas devém impedir que os Contra-Terroristas pguem os refens ou evitar de serem eliminados.</p>
              </div>
            </div>
          </div>

          <div class="col-xl-3 col-lg-6">
            <div class="row">

              <div class="col-10 mb-2">
                <h5 class="feature-title font-bold mb-1">Braço Direito</h5>
                <p class="grey-text mt-2">O modo braço direito do CSGO, conta com quase os mesmos mapas do modo competitivo, porém com um tamanho reduzido para apenas um dos bombsites, nele você enfrenta outros dois jogadores e o time que atingir 16 pontos primeiro é o vencedor</p>
              </div>
            </div>
          </div>

          <div class="col-xl-3 col-lg-6">
            <div class="row">

              <div class="col-10 mb-2">
                <h5 class="feature-title font-bold mb-1">Casual desarme</h5>
                <p class="grey-text mt-2">Queres jogar Counter-Strike, mas não queres comprometer-te em jogar uma partida completa? Procura uma partida casual e joga ao teu próprio ritmo. No modo Casual, os jogadores recebem armadura e kits de desativação automaticamente e ganham dinheiro adicional por matarem adversários.</p>
              </div>
            </div>
          </div>

          <div class="col-xl-3 col-lg-6">
            <div class="row">
              <div class="col-10 mb-2">
                <h5 class="feature-title font-bold mb-1">Competitivo</h5>
                <p class="grey-text mt-2">Esta é a jogabilidade clássica que tornou o CS:GO famoso. Entra uma partida 5 contra 5 no formato melhor de 30 com as regras padrão competitivas do Counter-Strike num dos mapas clássicos. Podes entrar numa partida sozinho ou formar uma equipa e entrar numa partida juntamente com os teus colegas.</p>
              </div>
            </div>
          </div>

        </div>

      </section>

    </div>
  </main>

</body>

</html>
