<?php
defined('BASEPATH') OR exit('No direct script acess allowed');

class EstoqueModel extends CI_Model
{
    public function getData()
    {
        $sql = "SELECT * FROM estoquemodel";

        $res = $this->db->query($sql);

        $data = $res->result_array();

        return $data[0];
    }

    public function getDataFromId($id)
    {
        $sql = "SELECT * FROM estoquemodel WHERE Id = $id";

        $res = $this->db->query($sql);

        $data = $res->result_array();

        return $data[0];
    }

    public function novoSetor()
    {
        if(sizeof($_POST) == 0) return;

        else
        {
            $data = $this->input->post();
            $this->db->insert('estoquemodel', $data);
        }
    }

    public function editarSetor($id)
    {
        $data = $this->getDataFromId($id);       
        
        $newdata = $this->input->post();

        if($data == $newdata) return;
        if($newdata == null) return;

        $this->db->set('title', $this->input->post('title'));
        $this->db->set('card_title', $this->input->post('card_title'));
        $this->db->set('image', $this->input->post('image'));
        $this->db->where('id', $id);
        $this->db->update('estoquemodel');
    }
}

        